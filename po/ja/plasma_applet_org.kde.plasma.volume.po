# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.volume\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-03 00:48+0000\n"
"PO-Revision-Date: 2019-07-10 18:11+0900\n"
"Last-Translator: Tomohiro Hyakutake <tomhioo@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 19.04.2\n"

#: contents/ui/DeviceListItem.qml:26
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:35
#, kde-format
msgid "Device name not found"
msgstr ""

#: contents/ui/ListItemBase.qml:70
#, kde-format
msgid "Currently not recording"
msgstr ""

#: contents/ui/ListItemBase.qml:71
#, kde-format
msgid "Currently not playing"
msgstr ""

#: contents/ui/ListItemBase.qml:184
#, fuzzy, kde-format
#| msgid "Show additional options for %1"
msgctxt "@action:button"
msgid "Additional Options"
msgstr "%1の追加オプションを表示"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "%1の追加オプションを表示"

#: contents/ui/ListItemBase.qml:203
#, fuzzy, kde-format
#| msgid "Mute %1"
msgctxt "@action:button"
msgid "Unmute"
msgstr "%1をミュート"

#: contents/ui/ListItemBase.qml:203
#, fuzzy, kde-format
#| msgid "Mute"
msgctxt "@action:button"
msgid "Mute"
msgstr "ミュート"

#: contents/ui/ListItemBase.qml:205
#, fuzzy, kde-format
#| msgid "Mute %1"
msgid "Unmute %1"
msgstr "%1をミュート"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "%1をミュート"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "%1 の ボリュームを調整"

#: contents/ui/ListItemBase.qml:283
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:303
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:32
#, kde-format
msgid "Audio Volume"
msgstr "音量"

#: contents/ui/main.qml:56
#, kde-format
msgid "Audio Muted"
msgstr "ミュート済み"

#: contents/ui/main.qml:58
#, kde-format
msgid "Volume at %1%"
msgstr "ボリューム %1%"

#: contents/ui/main.qml:75
#, kde-format
msgid "Middle-click to unmute"
msgstr ""

#: contents/ui/main.qml:76
#, kde-format
msgid "Middle-click to mute all audio"
msgstr ""

#: contents/ui/main.qml:237
#, fuzzy, kde-format
#| msgid "No output or input devices found"
msgid "No output device"
msgstr "入力や出力デバイスが見つかりません"

#: contents/ui/main.qml:369
#, kde-format
msgid "Increase Volume"
msgstr "音量を上げる"

#: contents/ui/main.qml:375
#, kde-format
msgid "Decrease Volume"
msgstr "音量を下げる"

#: contents/ui/main.qml:381
#, kde-format
msgid "Mute"
msgstr "ミュート"

#: contents/ui/main.qml:387
#, kde-format
msgid "Increase Microphone Volume"
msgstr "マイクの音量を上げる"

#: contents/ui/main.qml:393
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "マイクの音量を下げる"

#: contents/ui/main.qml:399
#, kde-format
msgid "Mute Microphone"
msgstr "マイクをミュート"

#: contents/ui/main.qml:503
#, kde-format
msgid "Devices"
msgstr "デバイス"

#: contents/ui/main.qml:510
#, kde-format
msgid "Applications"
msgstr "アプリケーション"

#: contents/ui/main.qml:531 contents/ui/main.qml:533 contents/ui/main.qml:759
#, kde-format
msgid "Force mute all playback devices"
msgstr ""

#: contents/ui/main.qml:567
#, kde-format
msgid "No output or input devices found"
msgstr "入力や出力デバイスが見つかりません"

#: contents/ui/main.qml:586
#, kde-format
msgid "No applications playing or recording audio"
msgstr "再生や録音をしているアプリケーションなし"

#: contents/ui/main.qml:737
#, kde-format
msgid "Raise maximum volume"
msgstr "最大音量を上げる"

#: contents/ui/main.qml:763
#, fuzzy, kde-format
#| msgid "No output or input devices found"
msgid "Show virtual devices"
msgstr "入力や出力デバイスが見つかりません"

#: contents/ui/main.qml:769
#, kde-format
msgid "&Configure Audio Devices…"
msgstr ""

#: contents/ui/StreamListItem.qml:24
#, kde-format
msgid "Stream name not found"
msgstr ""

#~ msgid "General"
#~ msgstr "一般"

#~ msgid "Volume step:"
#~ msgstr "音量ステップ:"

#, fuzzy
#~| msgid "Audio Volume"
#~ msgid "Audio volume"
#~ msgstr "音量"

#, fuzzy
#~| msgid "No output or input devices found"
#~ msgid "Default output device"
#~ msgstr "入力や出力デバイスが見つかりません"
